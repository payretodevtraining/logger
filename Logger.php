<?php
namespace Logger;

/**
 * Logger Class
 */
class Logger
{
    /**
     * @var boolean use DB for log
     */
    public static $useDB = false;

    /**
     * @var string Debug Type
     */
    const DEBUG = 'DEBUG';

    /**
     * @var string Info Type
     */
    const INFO = 'INFO';
    
    /**
     * @var string Notice Type
     */
    const NOTICE = 'NOTICE';
    
    /**
     * @var string Warning Type
     */
    const WARNING = 'WARNING';
    
    /**
     * @var string Error Type
     */
    const ERROR = 'ERROR';

    public function __construct()
    {
        # Registering shutdown function
        register_shutdown_function('fatalHandler');
    }

    /**
     * get Path for Log file based on date
     * @return String
     */
    public static function getLogPath()
    {
        return __DIR__ . '/Data/' . date('Y-m-d') . '_log.txt';
    }

    /**
     * Check if string is Json format or not
     * @param  string $string
     * @return boolean
     */
    public static function isJSON($string)
    {
        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }

    /**
     * Write Log to file based on their log level
     * @param  string $level
     * @param  string $message
     * @param  array  $context
     * @return void
     */
    public static function write($level, $message, $context = array())
    {
        $sessionID = session_id();

        if (self::$useDB) {
            self::logToDB($level, $message, $context, $sessionID);
        } else {
            self::logToFile($level, $message, $context, $sessionID);
        }
    }

    /**
     * write log into file
     * @param  string $level
     * @param  string $message
     * @param  string $context
     * @param  string $sessionID
     * @return void
     */
    public static function logToFile($level, $message, $context, $sessionID)
    {
        if (!empty($context)) {
            if (self::isJSON($context)) {
                $context = print_r(json_decode($context), true);
            } else {
                $context = print_r($context, true);
            }

            $log = sprintf(
                '[%s][%s][session_%s] %s => %s',
                date('Y-m-d H:i:s'),
                $level,
                $sessionID,
                $message,
                $context
            );
        } else {
            $log = sprintf(
                '[%s][%s][session_%s] %s'. "\r\n",
                date('Y-m-d H:i:s'),
                $level,
                $sessionID,
                $message
            );
        }

        file_put_contents(self::getLogPath(), $log, FILE_APPEND);
    }

    /**
     * write log into database, this function still on development
     * @param  string $level
     * @param  string $message
     * @param  string $context
     * @param  string $sessionID
     * @return void
     */
    public static function logToDB($level, $message, $context, $sessionID)
    {
        // run this query for each shop system
        // $sql = "INSERT INTO `Logger` (`log_date`, `log_level`, `log_session`, `log_message`, `log_data`)
        // VALUES ('', '', '', '', '');";
    }

    /**
     * function for log process as debug type
     * @param  string $message
     * @param  array  $context
     * @return void
     */
    public static function debug($message, $context = array())
    {
        self::write(self::DEBUG, $message, $context);
    }

    /**
     * function for log process as info type
     * @param  string $message
     * @return void
     */
    public static function info($message)
    {
        self::write(self::INFO, $message);
    }

    /**
     * function for log process as notice type
     * @param  string $message
     * @return void
     */
    public static function notice($message)
    {
        self::write(self::NOTICE, $message);
    }

    /**
     * function for log process as warning type
     * @param  string $message
     * @return void
     */
    public static function warning($message)
    {
        self::write(self::WARNING, $message);
    }

    /**
     * function for log process as error type
     * @param  string $message
     * @return void
     */
    public static function error($message)
    {
        self::write(self::ERROR, $message);
    }

    /**
     * Handling fatal error
     *
     * @return void
     */
    public static function fatalHandler()
    {
        # Getting last error
        $error = error_get_last();
     
        # Checking if last error is a fatal error
        if (($error['type'] === E_ERROR) || ($error['type'] === E_USER_ERROR)) {
            # Here we handle the error, displaying HTML, logging, ...
            self::error('Sorry, a serious error has occured in ' . $error['file']);
        }
    }
}
