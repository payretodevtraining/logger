### Query for create table
```
CREATE TABLE `logger` (
  `log_id` int(11) NOT NULL,
  `log_date` datetime NOT NULL,
  `log_level` varchar(255) NOT NULL,
  `log_session` text NOT NULL,
  `log_message` text NOT NULL,
  `log_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `Logger`
  ADD PRIMARY KEY (`log_id`);

ALTER TABLE `Logger`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;
```
